class Pane {
  static get inputProperties() {
    return ["font-size", "--pane-color", "--dot-spacing"];
  }

  static get contextOptions() {
    return { alpha: true };
  }

  drawRect(ctx, x, y, w, h, fill = null, stroke = null) {
    ctx.save();
    ctx.beginPath();
    ctx.rect(x, y, w, h);
    if (fill) {
      ctx.fillStyle = fill;
      ctx.fill();
    }
    if (stroke) {
      ctx.strokeStyle = stroke;
      ctx.stroke();
    }
    ctx.closePath();
    ctx.restore();
  }

  drawLine(ctx, x, y, x2, y2, stroke) {
    ctx.save();
    ctx.translate(0.5, 0.5);
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = stroke;
    ctx.stroke();
    ctx.restore();
  }

  drawCircle(ctx, x, y, radius, color, alpha) {
    ctx.save();
    ctx.globalAlpha = alpha;
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2);
    ctx.fillStyle = color;
    ctx.fill();
    ctx.restore();
  }

  paint(ctx, size, styleMap) {
    const fontSizeBase = 16;
    const fontSize = styleMap.get("font-size").value;
    const dotSpacing = styleMap.get("--dot-spacing").value;
    const paneColor = String(styleMap.get("--pane-color"));
    const settings = {
      colors: {
        bg: "#ccc",
        border: "#aaa",
        hl: "#eee",
        bgDark: paneColor,
        hlDark: "#ddd",
      },
      tab: {
        width: 100 + (fontSize - fontSizeBase) * 2,
        height: 8 + (fontSize - fontSizeBase) * (8 / fontSizeBase),
      },
    };

    // backgrounds
    this.drawRect(ctx, 0, 0, size.width, size.height, settings.colors.bg);
    this.drawRect(
      ctx,
      settings.tab.width,
      0,
      size.width - settings.tab.width,
      settings.tab.height,
      settings.colors.bgDark
    );

    // borders
    const tw = settings.tab.width,
      th = settings.tab.height;
    this.drawRect(
      ctx,
      0.5,
      0.5,
      size.width - 1,
      size.height - 1,
      null,
      settings.colors.border
    );
    this.drawLine(ctx, tw, 0, tw, th, settings.colors.border);
    this.drawLine(ctx, tw, 0, tw, th, settings.colors.border);
    this.drawLine(ctx, tw, th, size.width, th, settings.colors.border);

    // highlights
    this.drawLine(ctx, 1, 1, tw - 1, 1, settings.colors.hl);
    this.drawLine(ctx, tw, th + 1, size.width - 2, th + 1, settings.colors.hl);
    this.drawLine(ctx, tw + 1, 1, size.width - 2, 1, settings.colors.hlDark);

    // footer shape-sequence
    const incr = (fontSize - fontSizeBase) * (4 / fontSizeBase);
    const space = dotSpacing;
    const marginBottom = 5 + incr;
    const shrink = 0.9;
    const opacityStart = 1;
    const amount = 7;
    let r = 6 + incr,
      alpha;
    let x = size.width - space - r;
    [0, 1, 2, 3, 4, 5, 6].forEach((i) => {
      alpha = opacityStart - (opacityStart / amount) * i;
      this.drawCircle(
        ctx,
        x,
        size.height - marginBottom - r,
        r,
        paneColor,
        alpha
      );
      x -= r + space + r * shrink;
      r *= shrink;
    });
  }
}

registerPaint("pane", Pane);
